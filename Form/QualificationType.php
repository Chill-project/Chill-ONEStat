<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\ONEStatBundle\Entity\Qualification;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class QualificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('qualification', ChoiceType::class, array(
                    'choices' => array_combine(
                            Qualification::getPossibleQualifications(), 
                            Qualification::getPossibleQualifications()),
                ))
                ->add('since', DateType::class, array(
                    'widget' => 'single_text', 'format' => 'dd-MM-yyyy')
                )
                ->add('edd', ChoiceType::class, array(
                    'choices' => array(
                        'Valid' => true,
                        'Not valid' => false
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'choices_as_values' => true
                ))
                ->add('cv', ChoiceType::class, array(
                    'choices' => array(
                        'Valid' => true,
                        'Not valid' => false
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'choices_as_values' => true
                ))
                ;
    }
    
}
