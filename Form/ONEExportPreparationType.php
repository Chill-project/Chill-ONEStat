<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\EventBundle\Form\Type\PickEventType;
use Chill\EventBundle\Form\Type\PickRoleType;
use Chill\EventBundle\Form\Type\PickStatusType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;


/**
 * Allow to prepare an export for ONE : 
 * 
 * - ask the event types to be taken into account ;
 * - ask the roles to be taken into account as :
 *     - animator
 *     - children
 * - ask the statuses to be taken into account
 * - ask the events period to be taken into account (date from and date to)
 * 
 * The configurables options are : 
 * - `ONE_type` : define if the view concerns an "Ecole de devoirs" 
 * (value = `devoirs`) or a "Plaine de vacances" (value = `plaine`). 
 * This option change the statement in the view rendering the form.
 * 
 * This form should be rendered through the block `one_preparation`, 
 * embedded with this bundle.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class ONEExportPreparationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', PickEventType::class, array(
           'multiple' => true,
           'attr' => array('class' => 'chill-category-link-parent'),
           'placeholder' => "Sélectionnez un ou plusieurs types d'événements"
           ));
        
        if ($options['animator']) {
            $animatorBuilder = $builder->create('animator', FormType::class, array('compound' => true));
            
            if ($options['ONE_type'] === 'devoirs') {
                $animatorBuilder->add('roles', PickRoleType::class, array(
                   'active_only' => false,
                   'multiple' => true,
                   'attr' => array('class' => 'chill-category-link-child')
                ));
            } elseif ($options['ONE_type'] === 'plaine') {
                $animatorBuilder->add('roles_volunteers', PickRoleType::class, array(
                   'active_only' => false,
                   'multiple' => true,
                   'attr' => array('class' => 'chill-category-link-child')
                ));
                $animatorBuilder->add('roles_paid', PickRoleType::class, array(
                   'active_only' => false,
                   'multiple' => true,
                   'attr' => array('class' => 'chill-category-link-child')
                ));
            } else {
                throw new \LogicException('The ONE_type '.$options['ONE_type'].''
                      . ' is not expected');
            }
            
            $builder->add($animatorBuilder);
        }
        
        if ($options['child']) {
            
            $childBuilder = $builder->create('child', FormType::class, array('compound' => true));
            
            $childBuilder->add('roles', PickRoleType::class, array(
               'active_only' => false,
               'multiple' => true,
               'attr' => array('class' => 'chill-category-link-child')
            ));
            
            $ageBuilder = $childBuilder->create('age', FormType::class, array('compound' => true));
            
            $ageBuilder->add('birth_from', DateType::class, array(
                'widget' => 'single_text', 'format' => 'dd-MM-yyyy'
            ));
            $ageBuilder->add('birth_to', DateType::class, array(
                'widget' => 'single_text', 'format' => 'dd-MM-yyyy'
            ));
            
            if ($options['child_age_calculation'] === true){
                $childBuilder->add('age_calculation_date', DateType::class, array(
                    'widget' => 'single_text', 'format' => 'dd-MM-yyyy'
                ));
            }
            
            $childBuilder->add($ageBuilder);

            $builder->add($childBuilder);
            
        }
        
        // add statuses form
        $builder->add('statuses', PickStatusType::class, array(
           'active_only' => false,
           'multiple' => true,
           'attr' => array('class' => 'chill-category-link-child')
        ));
        
        // add dates
        $builder->add('date_from', DateType::class, array(
           'widget' => 'single_text', 'format' => 'dd-MM-yyyy'
        ));
        $builder->add('date_to', DateType::class, array(
           'widget' => 'single_text', 'format' => 'dd-MM-yyyy'
        ));
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('block_name', 'one_preparation');
        $resolver->setDefined('ONE_type')
              ->setAllowedValues('ONE_type', array('plaine', 'devoirs'))
              ->setRequired('ONE_type');
        $resolver->setDefined('animator')
                ->setAllowedTypes('animator', 'boolean')
                ->setRequired('animator');
        $resolver->setDefined('child')
                ->setAllowedTypes('child', 'boolean')
                ->setRequired('child');
        $resolver->setDefined('child_age_calculation')
                ->setAllowedTypes('child_age_calculation', 'boolean')
                ->setDefault('child_age_calculation', false);
    }
    
    public function buildView(\Symfony\Component\Form\FormView $view, \Symfony\Component\Form\FormInterface $form, array $options)
    {
        $view->one_type = $options['ONE_type'];
    }
}
