<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Entity;

use Chill\MainBundle\Entity\HasCenterInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class Qualification implements HasCenterInterface
{
    /**
     *
     * @var int
     */
    private $id;
    
    /**
     *
     * @var string
     */
    private $qualification;
    
    /**
     *
     * @var \DateTime
     */
    private $since;
    
    /**
     *
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $person;
   
    /**
     * if the qualification is valid for EDD (=Ecoles
     * de devoirs
     *
     * @var boolean
     */
    private $edd = false;

    /**
     * if the qualification is valid for CV (= Centre
     * de vacances
     * 
     * @var boolean
     */
    private $cv = false;
    
    const BREVET = 'brevet';
    const ASSIMILATED = 'assimil';
    const EQUIVALENT = 'equival';
    const NO_QUALIF = 'no_qualif';
    const STAGE = 'stage';
  
    
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    public function getSince()
    {
        return $this->since;
    }

    public function setQualification($qualification)
    {
        $this->qualification = $qualification;
        
        return $this;
    }

    public function setSince(\DateTime $since)
    {
        $this->since = $since;
        
        return $this;
    }
    
    /**
     * 
     * @return \Chill\PersonBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function setPerson(\Chill\PersonBundle\Entity\Person $person)
    {
        $this->person = $person;
        return $this;
    }
    
    public static function getPossibleQualifications()
    {
        return array(
            self::ASSIMILATED,
            self::BREVET,
            self::EQUIVALENT,
            self::NO_QUALIF,
            self::STAGE
            );
    }

    public function getCenter()
    {
        return $this->getPerson()->getCenter();
    }

    public function getEdd()
    { 
        return $this->edd;
    }

    public function setEdd($edd)
    {
        $this->edd = $edd;
        
        return $this;
    }

    public function getCv()
    {
        return $this->cv;
    }

    public function setCv($cv)
    {
        $this->cv = $cv;
    
        return $this;
    }
    
    public function blockStageOnEDD(ExecutionContextInterface $context) 
    {
        if ($this->getEdd() === true && $this->getQualification() === self::STAGE) {
            $context->buildViolation('La qualification "stage" n\'est pas permise '
                  . 'pour les écoles de devoir')
                  ->atPath('edd')
                  ->addViolation();
        }
    }
    
    public function ensureAtLeastOneDomain(ExecutionContextInterface $context)
    {
        if ($this->getEdd() === false && $this->getCv() === false) {
            $context->buildViolation('La qualification doit au être valable '
                  . 'pour les écoles de devoir ou pour les centres de vacances.')
                  ->addViolation();
        }
    }
}
