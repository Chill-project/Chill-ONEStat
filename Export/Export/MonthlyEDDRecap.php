<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Chill\EventBundle\Entity\Role as EventRole;
use Chill\EventBundle\Entity\EventType;
use Doctrine\DBAL\Types\Type;
use Chill\EventBundle\Entity\Status;

/**
 * Provide the monthly recap for "école de devoirs"
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class MonthlyEDDRecap implements ExportInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'devoirs',
           'animator' => true,
            'child' => true
        ));
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getDescription()
    {
        return "Fournit le 'récapitulatif mensuel' demandé par l'ONE pour "
        . "écoles de devoir.";
    }

    public function getLabels($key, array $values, $data)
    {
        switch ($key) {
            case 'date': 
                return function( $value) { 
                    return $value === '_header' ? 'Date' : $value->format('d-m-Y'); 
                };
                break;
            case 'nb_child': 
                $h = "Nombre d'enfants de 6 à 15 ans accueillis";
                break;
            case 'animators_with_qualification':
                $h = "Nombre d'animateurs qualifiés présents (y compris coord.)";
                break;
            case 'animators_without_qualification':
                $h = "Nombre d'animateurs non qualifiés présents";
                break;
            case 'animators_total':
                $h = "Nombre total d'animateurs présents";
                break;
            default:
                throw new \LogicException("header not found for key $key");
        }
        
        return function($value) use ($h) { 
            return $value === '_header' ? $h : $value; 
        };
    }

    public function getQueryKeys($data)
    {
        return  array('date', 'nb_child', 'animators_with_qualification', 
            'animators_without_qualification', 'animators_total');
    }

    public function getResult($query, $data)
    {
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Récapitulatif mensuel (Annexe S)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        $participantsList = $this->getParticipantsListSQL();
        $query = $this->getMonthlyRecapSQL();
        
        $sql = "WITH participants_list AS (".$participantsList.") ".$query." ORDER BY event_date";
        
        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addScalarResult('event_date', 'date', Type::DATE)
                ->addScalarResult('nb_child', 'nb_child')
                ->addScalarResult('animators_with_qualification', 'animators_with_qualification')
                ->addScalarResult('animators_without_qualification', 'animators_without_qualification')
                ->addScalarResult('animators_total', 'animators_total')
                ;
        
        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        
        return $this->handleParameters($query, $data, $acl);
                
        
        return $query;
        
    }
    
    protected function handleParameters($query, array $data, array $acl)
    {
        // get the parameters for the query
        $dateFrom = $data['one']['date_from'];//->format('Y-m-d');
        $dateTo = $data['one']['date_to'];//->format('Y-m-d');
        $roleChildIds = array_map(
                function(EventRole $r) { return $r->getId(); }, 
                $data['one']['child']['roles']->toArray()
                );
        $roleAnimatorIds = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles']->toArray()
                );
        $eventTypesIds = array_map(
                function (EventType $t) { return $t->getId(); },
                $data['one']['type']->toArray()
                );
        $statusesIds = array_map(
                function (Status $s) { return $s->getId(); },
                $data['one']['statuses']->toArray()
                );
        $authorizedCentersIds = array_map(
            function ($el) { return $el['center']->getId(); },
            $acl
            );
            
        $query
                ->setParameter('event_types', $eventTypesIds)
                ->setParameter('role_animator', $roleAnimatorIds)
                ->setParameter('role_child', $roleChildIds)
                ->setParameter('birthdate_to', $data['one']['child']['age']['birth_to'], Type::DATE)
                ->setParameter('birthdate_from', $data['one']['child']['age']['birth_from'], Type::DATE)
                ->setParameter('date_from', $dateFrom, Type::DATE)
                ->setParameter('date_to', $dateTo, Type::DATE)
                ->setParameter('centers', $authorizedCentersIds)
                ->setParameter('statuses', $statusesIds)
                ;
        
        return $query;
    }
    
    protected function getParticipantsListSQL()
    {
        // metadata
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');
        
        //tables
        $participationTable = $participationMtd->getTableName();
        $eventTable = $eventMtd->getTableName();
        
        //field
        $eventDate = $eventMtd->getColumnName('date');
        $eventId = $eventMtd->getColumnName('id');
        
        // most used statement (table and columns)
        $participationToEvent = $participationMtd->getAssociationMapping('event')['joinColumns'][0]['name'];
        $participationToPerson = $participationMtd->getAssociationMapping('person')['joinColumns'][0]['name'];
        $participationToRole = $participationMtd->getAssociationMapping('role')['joinColumns'][0]['name'];
        $participationToStatus = $participationMtd->getAssociationMapping('status')['joinColumns'][0]['name'];
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
        
        return <<<EOT
SELECT 
	participation.$participationToPerson AS person_id, 
        participation.$participationToRole AS role_id,
        event.$eventDate AS event_date,
        get_last_qualification_edd(participation.person_id, event.date) AS qualification
FROM 	$participationTable AS participation 
JOIN 	$eventTable AS event ON participation.$participationToEvent = event.$eventId
WHERE 
	-- event_date
	event.$eventDate BETWEEN :date_from::date AND :date_to::date
	AND
        -- event centers
        event.$eventToCenter IN (:centers)
        AND
	-- event type pour "école de devoir"
	event.$eventToType IN (:event_types)
	AND
	-- participation status
	participation.$participationToStatus IN (:statuses)
	AND
                (
                --role for animators
                participation.$participationToRole IN (:role_animator)
                OR
                -- role for children
                participation.$participationToRole IN (:role_child)
                )
EOT;
    }
    
    protected function getMonthlyRecapSQL()
    {
        // metadata
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');
        
        //tables
        $personTable = $personMtd->getTableName();
        
        //field
        $personId = $personMtd->getColumnName('id');
        $personBirthdate = $personMtd->getColumnName('birthdate');
        
        
        return <<<EOT
SELECT 
	list_dates.event_date AS event_date, 
	CASE WHEN sq_nb_children.nb_child IS NULL THEN 0::bigint ELSE sq_nb_children.nb_child END AS nb_child, 
	CASE WHEN sq_nb_qualif_animator.animators_with_qualification IS NULL THEN 0::bigint ELSE sq_nb_qualif_animator.animators_with_qualification END AS animators_with_qualification, 
	CASE WHEN sq_nb_not_qualif_animator.animators_without_qualification IS NULL THEN 0::bigint ELSE sq_nb_not_qualif_animator.animators_without_qualification END AS animators_without_qualification,
        -- addition which take null into account
        CASE WHEN sq_nb_not_qualif_animator.animators_without_qualification IS NULL THEN 0::bigint ELSE sq_nb_not_qualif_animator.animators_without_qualification END 
        + 
        CASE WHEN sq_nb_qualif_animator.animators_with_qualification IS NULL THEN 0 ELSE sq_nb_qualif_animator.animators_with_qualification END
        AS animators_total
FROM
       ( 
	SELECT 
		generate_series(:date_from::date, :date_to::date, '1 day'::interval)::date AS event_date
	)  list_dates

LEFT JOIN
	(
	-- query for children
	SELECT 
		COUNT(*) AS nb_child, 
		event_date AS event_date
	FROM participants_list
	JOIN $personTable AS person ON participants_list.person_id = person.$personId
	WHERE 
		-- role for children
		participants_list.role_id IN (:role_child)
		AND
		person.$personBirthdate BETWEEN :birthdate_from::date AND :birthdate_to::date
		
	GROUP BY event_date
	) AS sq_nb_children ON list_dates.event_date = sq_nb_children.event_date
LEFT JOIN 
	(
	-- query for animator with brevet
	SELECT  
		COUNT(*) AS animators_with_qualification, 
		event_date AS event_date
	FROM participants_list
	WHERE 
		-- role for animator
		participants_list.role_id IN (:role_animator)
                AND
		participants_list.qualification IN ('assimil', 'brevet', 'equival')
	GROUP BY event_date
	) AS sq_nb_qualif_animator ON sq_nb_qualif_animator.event_date = list_dates.event_date
LEFT JOIN 
	(
	-- query for animator **without** brevet
	SELECT  
		COUNT(*) AS animators_without_qualification,
		event_date AS event_date
	FROM participants_list 
	WHERE 
		-- role for animator
		participants_list.role_id IN (:role_animator)
                AND
		(
                    participants_list.qualification NOT IN ('assimil', 'brevet', 'equival')
                    OR
                    participants_list.qualification IS NULL
                )
	GROUP BY event_date
	) AS sq_nb_not_qualif_animator ON sq_nb_not_qualif_animator.event_date = list_dates.event_date
EOT;
    }

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }

}
