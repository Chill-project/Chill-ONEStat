<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Chill\EventBundle\Entity\Role as EventRole;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;

/**
 * Create the "liste des participants" required for ONE / Annexe III
 * (plaine de vacances)
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ParticipantsListCV implements ExportInterface
{
    
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     * The keys used in the query
     *
     * @var string[]
     */
    protected $queryKeys = array(
        'name' => 'Nom et prénom',
        'age'  => 'Âge',
        'nb_participations' => 'Nombre de journées'
    );

    public function __construct(
                EntityManagerInterface $em
            )
    {
        $this->entityManager = $em;
    }

    public function getDescription()
    {
        return "Fournit la \"Liste des enfants accueillis\" demandé par l'ONE "
        . "pour les centres de vacances.";
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'plaine',
            'child' => true,
            'child_age_calculation' => true,
            'animator' => false
        ));
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getLabels($key, array $values, $data)
    {
        return function($value) use ($key) {
            return $value === '_header' ? $this->queryKeys[$key] : $value;
        };
    }

    public function getQueryKeys($data)
    {
        return array_keys($this->queryKeys);
    }

    public function getResult($query, $data)
    {
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Liste des enfants accueillis (Annexe III)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        // get the parameters for the query
        $dateFrom =         $data['one']['date_from'];//->format('Y-m-d');
        $dateTo =           $data['one']['date_to'];//->format('Y-m-d');
        $birthdateFrom =    $data['one']['child']['age']['birth_from'];
        $birthdateTo =      $data['one']['child']['age']['birth_to'];
        $ageCalculationDate = $data['one']['child']['age_calculation_date'];
        $roleChildIds = array_map(
                function(EventRole $r) { return $r->getId(); }, 
                $data['one']['child']['roles']->toArray()
                );
        $eventTypesIds = array_map(
                function (EventType $t) { return $t->getId(); },
                $data['one']['type']->toArray()
                );
        $statusesIds = array_map(
                function (Status $s) { return $s->getId(); },
                $data['one']['statuses']->toArray()
                );
        $authorizedCentersIds = array_map(
            function ($el) { return $el['center']->getId(); },
            $acl
            );
        
        // store table string in string
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');
        // table name
        $personTable = $personMtd->getTableName();
        $participationTable = $participationMtd->getTableName();
        $eventTable = $eventMtd->getTableName();
        // field
        $eventDate = $eventMtd->getColumnName('date');
        $eventId = $eventMtd->getColumnName('id');
        $personFirstname = $personMtd->getColumnName('firstname');
        $personLastname = $personMtd->getColumnName('lastname');
        $personBirthdate = $personMtd->getColumnName('birthdate');
        // joins between table
        $participationToEvent = $participationMtd->getAssociationMapping('event')['joinColumns'][0]['name'];
        $participationToPerson = $participationMtd->getAssociationMapping('person')['joinColumns'][0]['name'];
        $participationToRole = $participationMtd->getAssociationMapping('role')['joinColumns'][0]['name'];
        $participationToStatus = $participationMtd->getAssociationMapping('status')['joinColumns'][0]['name'];
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
            
        $sql = <<<EOT
SELECT
	person.$personLastname || ' ' ||person.$personFirstname as name,
	EXTRACT(YEAR FROM age(:age_calculation_date::date, person.$personBirthdate)) AS age,
        COUNT(sb_list_participations.event_date) AS nb_participations
FROM
	$personTable AS person
JOIN
	(
	SELECT DISTINCT
                participation.$participationToPerson as person_id, 
                event.$eventDate AS event_date
	FROM 
		$participationTable AS participation
	JOIN
		$eventTable AS event ON participation.$participationToEvent = event.$eventId
	WHERE
		event.$eventToType IN (:event_type_ids)
		AND
		participation.$participationToStatus IN (:statuses_ids)
		AND
		event.$eventDate BETWEEN :date_from::date AND :date_to::date
		AND
		participation.$participationToRole IN (:role_ids)
		AND
		event.$eventToCenter IN (:center_ids)
	) AS sb_list_participations ON sb_list_participations.person_id = person.id
WHERE
        person.$personBirthdate BETWEEN :birthdate_from::date AND :birthdate_to::date
GROUP BY name, age
ORDER BY name

EOT;
        
        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addScalarResult('name', 'name')
                ->addScalarResult('nb_participations', 'nb_participations')
                ->addScalarResult('age', 'age');
        
        return $this->entityManager->createNativeQuery($sql, $rsm)
                ->setParameter('statuses_ids', $statusesIds)
                ->setParameter('event_type_ids', $eventTypesIds)
                ->setParameter('center_ids', $authorizedCentersIds)
                ->setParameter('date_from', $dateFrom)
                ->setParameter('date_to', $dateTo)
                ->setParameter('role_ids', $roleChildIds)
                ->setParameter('age_calculation_date', $ageCalculationDate)
                ->setParameter('birthdate_from', $birthdateFrom)
                ->setParameter('birthdate_to', $birthdateTo)
                ;
        
        

    }

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }

}
