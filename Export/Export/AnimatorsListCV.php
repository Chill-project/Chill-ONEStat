<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Chill\EventBundle\Entity\Role as EventRole;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;
use Doctrine\DBAL\Types\Type;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AnimatorsListCV implements ExportInterface
{
    
        
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     * The keys used in the query
     *
     * @var string[]
     */
    protected $queryKeys = array(
        'firstname' => 'Prénom',
        'lastname' =>  'Nom',
        'birthdate' => 'Date de naissance',
        'stage' => 'Animateur en stage de 2ième cycle',
        'brevet' => 'Brevet ou équivalence au brevet',
        'assimil' => 'Animateur assimilé',
        'paid' => 'Indemnisé',
        'volunteer' => 'Bénévole',
        'nb_days' => 'Nombre de jour prestés',
        'list_dates' => 'Date des prestations'
    );

    public function __construct(
                EntityManagerInterface $em
            )
    {
        $this->entityManager = $em;
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'plaine', 
           'animator' => true,
           'child' => false
        ));  
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getDescription()
    {
        return "Fournit la liste \"personnel d'encadrement\" demandé par l'ONE"
        . "pour les centres de vacances (Annexe III).";
    }

    public function getLabels($key, array $values, $data)
    {
        switch($key) {
            case 'birthdate':
                return function($value) {
                    if ($value === '_header') {
                        return $this->queryKeys['birthdate'];
                    } else {
                        return is_null($value) ? 
                            "Date de naissance inconnue" : $value->format('d-m-Y');
                    }
                };
            default:
                return function($value) use ($key) { 
                
                    return $value === '_header' ? $this->queryKeys[$key] : $value; 
                };
        }
    }

    public function getQueryKeys($data)
    {
        return array_keys($this->queryKeys);
    }

    public function getResult($query, $data)
    {
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Personnel d'encadrement (Annexe III)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
         // get the parameters for the query
        $dateFrom = $data['one']['date_from'];//->format('Y-m-d');
        $dateTo = $data['one']['date_to'];//->format('Y-m-d');
        $roleAnimatorVolunteersIds = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles_volunteers']->toArray()
                );
        $roleAnimatorPaidIds = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles_paid']->toArray()
                );
        $eventTypesIds = array_map(
                function (EventType $t) { return $t->getId(); },
                $data['one']['type']->toArray()
                );
        $statusesIds = array_map(
                function (Status $s) { return $s->getId(); },
                $data['one']['statuses']->toArray()
                );
        $authorizedCentersIds = array_map(
            function ($el) { return $el['center']->getId(); },
            $acl
            );
        
        // store table string in string
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');

        // table name
        $personTable = $personMtd->getTableName();
        $participationTable = $participationMtd->getTableName();
        $eventTable = $eventMtd->getTableName();
        
        // field
        $eventDate = $eventMtd->getColumnName('date');
        $eventId = $eventMtd->getColumnName('id');
        $personFirstname = $personMtd->getColumnName('firstname');
        $personLastname = $personMtd->getColumnName('lastname');
        $personBirthdate = $personMtd->getColumnName('birthdate');
        $personId = $personMtd->getColumnName('id');
        
        // joins between table
        $participationToEvent = $participationMtd->getAssociationMapping('event')['joinColumns'][0]['name'];
        $participationToPerson = $participationMtd->getAssociationMapping('person')['joinColumns'][0]['name'];
        $participationToRole = $participationMtd->getAssociationMapping('role')['joinColumns'][0]['name'];
        $participationToStatus = $participationMtd->getAssociationMapping('status')['joinColumns'][0]['name'];
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
        
        
        $sql = <<<EOT
WITH qualification_on_participation AS (SELECT 
	participation.*, 
        get_last_qualification_cv(participation.$participationToPerson, event.$eventDate) AS qualification
FROM 	$participationTable AS participation 
JOIN 	$eventTable AS event ON participation.$participationToEvent = event.$eventId
WHERE 
	-- event_date
	event.$eventDate BETWEEN :date_from::date AND :date_to::date
	AND
        -- event centers
        event.$eventToCenter IN (:centers)
        AND
	-- event type pour "centre de vacances"
	event.$eventToType IN (:event_types)
	AND
	-- participation status
	participation.$participationToStatus IN (:statuses)
	AND
	(
		--role for volunteers
		participation.$participationToRole IN (:role_animator_volunteer)
		OR
		--role for "indemnisé"
		participation.$participationToRole IN (:role_animator_paid)
	)
)
SELECT DISTINCT
	person.$personLastname AS lastname,
	person.$personFirstname AS firstname,
	person.$personBirthdate AS birthdate,
	CASE qualification_on_participation.qualification WHEN 'stage'  THEN 'x' ELSE '' END AS stage,
	CASE WHEN qualification_on_participation.qualification IN ('brevet', 'equival') THEN 'x' ELSE '' END as brevet,
        CASE qualification_on_participation.qualification WHEN 'assimil' THEN 'x' ELSE '' END as assimil,
	CASE WHEN qualification_on_participation.$participationToRole IN (:role_animator_volunteer) THEN 'x' ELSE '' END AS volunteer,
	CASE WHEN qualification_on_participation.$participationToRole IN (:role_animator_paid) THEN 'x' ELSE '' END AS paid,
	count_sb.nb_days AS nb_days,
	agg_sb.list_dates AS list_dates
FROM qualification_on_participation
JOIN $personTable AS person ON person.$personId = qualification_on_participation.$participationToPerson
JOIN (
	SELECT $participationToPerson AS person_id, qualification, $participationToRole AS role_id, COUNT(event.$eventDate) AS nb_days
	FROM qualification_on_participation
	JOIN $eventTable AS event ON event.$eventId = qualification_on_participation.$participationToEvent
	GROUP BY $participationToPerson, qualification, $participationToRole
) AS count_sb 
	ON 
		count_sb.person_id = person.$personId
		AND
		count_sb.qualification = qualification_on_participation.qualification
		AND
		count_sb.role_id = qualification_on_participation.role_id
JOIN (
	SELECT 
		qualification_on_participation.$participationToPerson AS person_id, 
		$participationToRole AS role_id, 
		qualification,
		string_agg(to_char(event.$eventDate, 'DD-MM-YYYY'), ', ' ) AS list_dates
	FROM qualification_on_participation
	JOIN $eventTable AS event ON event.$eventId = qualification_on_participation.$participationToEvent
	GROUP BY $participationToPerson, $participationToRole, qualification	
) AS agg_sb
	ON 
		agg_sb.person_id = person.id
		AND
		agg_sb.qualification = qualification_on_participation.qualification
		AND
		agg_sb.role_id = qualification_on_participation.$participationToRole
ORDER BY lastname, firstname

EOT;
        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addScalarResult('lastname', 'lastname')
              ->addScalarResult('firstname', 'firstname')
              ->addScalarResult('birthdate', 'birthdate', Type::DATE)
              ->addScalarResult('stage', 'stage')
              ->addScalarResult('brevet', 'brevet')
              ->addScalarResult('assimil', 'assimil')
              ->addScalarResult('volunteer', 'volunteer')
              ->addScalarResult('paid', 'paid')
              ->addScalarResult('nb_days', 'nb_days')
              ->addScalarResult('list_dates', 'list_dates')
              ;
        
        $query = $this->entityManager->createNativeQuery($sql, $rsm)
            ->setParameter('event_types', $eventTypesIds)
            ->setParameter('role_animator_volunteer', $roleAnimatorVolunteersIds)
            ->setParameter('role_animator_paid', $roleAnimatorPaidIds)
            ->setParameter('date_from', $dateFrom, Type::DATE)
            ->setParameter('date_to', $dateTo, Type::DATE)
            ->setParameter('centers', $authorizedCentersIds)
            ->setParameter('statuses', $statusesIds)
              ;
        return $query;
    }

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }

}
