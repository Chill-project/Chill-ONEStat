<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\CustomFieldsBundle\Entity\CustomField;

/**
 * Provide the monthly recap for "école de devoirs"
 *
 * @author Champs Libres <info@champs-libres.coop>
 */
class ParticipantsList implements ExportInterface
{
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    protected $keyHeader = array(
        'name' => 'Nom et prénom',
        'municipality' => 'Commune de résidence',
        'birthdate' => 'Année de naissance',
        'school' => 'École fréquentée'
    );
    
    /**
     *
     * @var CustomFieldProvider
     */
    protected $customFieldProvider;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     * The custom field entity containing the school.
     *
     * @var \Chill\CustomFieldsBundle\Entity\CustomField
     */
    protected $cfSchool;
    
    /**
     * 
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $em
     * @param CustomFieldProvider $cfProvider
     * @param TranslatableStringHelper $translatableStringHelper
     * @param type $cfSchoolSlug the slug of the custom field containing the school
     */
    public function __construct(
            TranslatorInterface $translator,
            EntityManagerInterface $em,
            CustomFieldProvider $cfProvider,
            TranslatableStringHelper $translatableStringHelper,
            $cfSchoolSlug
            )
    {
        $this->translator = $translator;
        $this->entityManager = $em;
        $this->customFieldProvider = $cfProvider;
        $this->cfSchool = $em->getRepository(CustomField::class)
              ->findOneBy(array('slug' => $cfSchoolSlug));
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'devoirs',
            'child' => true,
            'animator' => false
        ));
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getDescription()
    {
        return "Fournit la 'Liste nominative des enfants de 6 à 15 ans accueillis par "
        . "site d’accueil' demandé par l'ONE pour écoles de devoir.";
    }

    public function getLabels($key, array $values, $data)
    {
        switch($key) {
            case 'birthdate' : 
                return function ($value) {
                    return $value === '_header' ? $this->keyHeader['birthdate'] : 
                          $value->format('Y');
                };
            case 'school':
                return function ($value) {
                    if($value === '_header') {
                        return $this->keyHeader['school'];
                    }
                    
                    if (!isset($value[$this->cfSchool->getSlug()])) {
                        return '<aucune école renseignée>';
                    }
                    
                    $school = $this->customFieldProvider
                          ->getCustomFieldByType('long_choice')
                          ->deserialize(
                                $value[$this->cfSchool->getSlug()], 
                                $this->cfSchool);
                    
                    return $this->translatableStringHelper
                          ->localize($school->getText());
                };
            case 'municipality':
                return function ($value) use ($data) {
                    if ($value === null) {
                        return sprintf(
                                "Pas d'adresse valide au %s",
                                $data['one']['date_to']->format('d-m-Y')
                              );
                    }
                    
                    if ($value === '_header') {
                        return $this->keyHeader['municipality'];
                    }
                    
                    return $value;
                };
            default:
                if (array_key_exists($key, $this->keyHeader)) {
                    $h = $this->keyHeader[$key];
                } else {
                    $h = $key;
                }
                return function ($value) use ($h) {
                    return $value === '_header' ? $h : $value;
                };
        }
    }

    public function getQueryKeys($data)
    {
        return array_keys($this->keyHeader);
    }

    public function getResult($qb, $data)
    {
        $query = $qb->getQuery();
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Liste des participants (Annexe S)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(
        array $requiredModifiers,
        array $acl,
        array $data = array()
    ) {
        $authorizedCenters = array_map(
            function ($el) {
                return $el['center'];
            },
            $acl
        );
        $qb = $this->entityManager->createQueryBuilder();
        
        $dateFrom = $data['one']['date_from'];
        $dateTo = $data['one']['date_to'];
        
        $qbDistinctSelectedPerson = $this->entityManager->createQueryBuilder();
        $qbDistinctSelectedPerson->select(array('pe.id'))
            ->from('Chill\EventBundle\Entity\Participation', 'pa')
            ->innerJoin('pa.event', 'ev')
            ->innerJoin('pa.person', 'pe')
            ->where('ev.type IN (:selectedEventTypes)')
            ->andWhere('ev.center IN (:authorizedCenters)')
            ->andWhere('pa.role IN (:childRoles)')
            ->andWhere('pa.status IN (:selectedPartiticipationStatus)')
            ->andWhere($qbDistinctSelectedPerson->expr()->between('pe.birthdate', ':startBirthdate15YearOld', ':stopBirthdate6YearOld'))
            ->andWhere($qbDistinctSelectedPerson->expr()->between('ev.date', ':dateFrom', ':dateTo'))
            ->groupBy('pe.id')
            ;

        $qbLastAdress = clone $qb;
        $qbLastAdress->select(array('MAX(ad.validFrom)'))
            ->from('Chill\PersonBundle\Entity\Person', 'adPe')
            ->leftJoin('adPe.addresses', 'ad')
            ->where('pers.id = adPe.id')
            ->andWhere($qbLastAdress->expr()->lte('ad.validFrom', ':dateTo'))
            ->groupBy('adPe.id')
            ;
        
        return $qb->select(array(
                'CONCAT(pers.firstName, \' \', pers.lastName) as name',
                'pers.birthdate as birthdate',
                'pers.cFData as school',
                'poco.name AS municipality'))
            ->from('Chill\PersonBundle\Entity\Person', 'pers')
            ->leftJoin('pers.addresses', 'addr')
            ->leftJoin('addr.postcode', 'poco')
            ->where($qb->expr()->in('pers.id', $qbDistinctSelectedPerson->getDql()))
            ->andWhere(
                $qb->expr()->orx(
                    $qb->expr()->isNull('addr.id'),
                    $qb->expr()->in('addr.validFrom', $qbLastAdress->getDql())
                )
            )
            ->setParameter('selectedEventTypes', $data['one']['type'])
            ->setParameter('authorizedCenters', $authorizedCenters)
            ->setParameter('childRoles', $data['one']['child']['roles'])
            ->setParameter('dateFrom', $dateFrom)
            ->setParameter('dateTo', $dateTo)
            ->setParameter('selectedPartiticipationStatus', $data['one']['statuses'])
            ->setParameter('stopBirthdate6YearOld', $data['one']['child']['age']['birth_to'])
            ->setParameter('startBirthdate15YearOld', $data['one']['child']['age']['birth_from'])
            ;
    }

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }
}
