<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * This report extends monthly recap to add a yearly coverage
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class YearlyEDDRecap extends MonthlyEDDRecap
{
    
    public function getTitle()
    {
        return "Récapitulatif annuel (Annexe S)";
    }
    
    public function getDescription()
    {
        return "Fournit le 'récapitulatif annuel' demandé par l'ONE pour "
        . "écoles de devoir.";
    }
    
    public function getQueryKeys($data)
    {
        return  array('month_year', 'nb_open_days', 'sum_child', 
            'sum_animators_with_qualification', 'sum_animators_without_qualification',
            'sum_animators_total');
    }
    
    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        // metadata
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        
        //tables
        $eventTable = $eventMtd->getTableName();
        
        //field
        $eventDate = $eventMtd->getColumnName('date');
        
        // most used statement (table and columns)
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
        
        $participantList = $this->getParticipantsListSQL() ;
        $monthlyRecapSQL = $this->getMonthlyRecapSQL();
        
        $sql = <<<EOT
WITH participants_list AS ($participantList),
monthly_recap AS ($monthlyRecapSQL)
SELECT monthly_recap.*, nb_open_days.nb_open_days
FROM
    (
        SELECT 
            EXTRACT(MONTH FROM event_date) || '-' || EXTRACT(YEAR FROM event_date) AS month_year,
            SUM(nb_child) AS sum_child,
            SUM(animators_with_qualification) AS sum_animators_with_qualification,
            SUM(animators_without_qualification) AS sum_animators_without_qualification,
            SUM(animators_total) AS sum_animators_total
        FROM
            monthly_recap AS monthly_recap
        GROUP BY month_year
        ORDER BY month_year DESC)  AS monthly_recap
LEFT JOIN

        (
            SELECT
                EXTRACT(MONTH FROM event.$eventDate) || '-' || EXTRACT(YEAR FROM event.$eventDate) AS month_year,
                COUNT(event.$eventDate) as nb_open_days
            FROM $eventTable AS event
            WHERE
                -- event_date
                event.$eventDate BETWEEN :date_from::date AND :date_to::date
                AND
                -- event centers
                event.$eventToCenter IN (:centers)
                AND
                -- event type pour "école de devoir"
                event.$eventToType IN (:event_types)
            GROUP BY month_year
		) AS nb_open_days ON nb_open_days.month_year = monthly_recap.month_year
EOT;
        
        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addScalarResult('month_year', 'month_year')
                ->addScalarResult('nb_open_days', 'nb_open_days')
                ->addScalarResult('sum_child', 'sum_child')
                ->addScalarResult('sum_animators_with_qualification', 'sum_animators_with_qualification')
                ->addScalarResult('sum_animators_without_qualification', 'sum_animators_without_qualification')
                ->addScalarResult('sum_animators_total', 'sum_animators_total')
                ;
        
        $query = $this->entityManager->createNativeQuery($sql, $rsm);
        
        return $this->handleParameters($query, $data, $acl);
    }
    
    public function getLabels($key, array $values, $data)
    {
        switch ($key) {
            case 'month_year':
                $h = "Mois et année";
                break;
            case "nb_open_days":
                $h = "Nombre de jours d'ouverture";
                break;
            case "sum_child":
                $h = "Nombre d’enfants de 6 à 15 ans accueillis";
                break;
            case "sum_animators_with_qualification":
                $h = "Nombre d’animateurs qualifiés présents";
                break;
            case "sum_animators_without_qualification":
                $h = "Nombre d’animateurs non qualifiés présents";
                break;
            case "sum_animators_total":
                $h = "Nombre total d’animateurs présents";
                break;
            default:
                throw new \Exception("the key $key is not known in this method ".
                      __METHOD__);
        }
        
        return function ($v)  use ($h) { return $v === '_header' ? $h : $v; };
    }
}
