<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\DBAL\Types\Type;
use Chill\EventBundle\Entity\Role as EventRole;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AnimatorsList implements ExportInterface
{
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     * the keys present in the query
     *
     * @var string[]
     */
    protected $queryKeys = array(
        'name' => 'Nom et prénom', 
        'year_of_birthdate' => 'Année de naissance', 
        'brevet' => 'Qualifié par Brevet', 
        'assimil' => 'Qualifié par assimilation', 
        'equival' => 'Qualifié par équivalence', 
        'non_qualified' => 'Non qualifié'
        );
    
    public function __construct(
            TranslatorInterface $translator,
            EntityManagerInterface $em
            )
    {
        $this->translator = $translator;
        $this->entityManager = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'devoirs', 
           'animator' => true,
            'child' => false
        ));    
        
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getDescription()
    {
        return "Fournit la 'Liste nominative de l’équipe d’animation par site d’accueil' demandé par l'ONE pour "
        . "les écoles de devoir.";
    }

    public function getLabels($key, array $values, $data)
    {

        return function($value) use ($key) {
            if ($value == '_header' ) {
                return $this->queryKeys[$key];
            } else {
                return $value;
            }
        };
    }

    public function getQueryKeys($data)
    {
        return array_keys($this->queryKeys);
    }

    public function getResult($query, $data)
    {
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Liste des animateurs (Annexe S)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        $authorizedCenters = array_map(function($el) { return $el['center']; }, $acl);
        
        $dateFrom = $data['one']['date_from'];
        $dateTo = $data['one']['date_to'];
        $roleAnimator = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles']->toArray()
                );
        $eventTypesIds = array_map(
                function (EventType $t) { return $t->getId(); },
                $data['one']['type']->toArray()
                );
        $statusesIds = array_map(
                function (Status $s) { return $s->getId(); },
                $data['one']['statuses']->toArray()
                );
        $authorizedCentersIds = array_map(
            function ($el) { return $el['center']->getId(); },
            $acl
            );
        
        // store table string in string
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');

        // table name
        $participationTable = $participationMtd->getTableName();
        $eventTable = $eventMtd->getTableName();
        $personTable = $personMtd->getTableName();
        
        // field
        $eventDate = $eventMtd->getColumnName('date');
        $eventId = $eventMtd->getColumnName('id');
        $personId = $personMtd->getColumnName('id');
        $personFirstname = $personMtd->getColumnName('firstname');
        $personLastname  = $personMtd->getColumnName('lastname');
        $personBirthdate = $personMtd->getColumnName('birthdate');
        
        // joins between table
        $participationToEvent = $participationMtd->getAssociationMapping('event')['joinColumns'][0]['name'];
        $participationToPerson = $participationMtd->getAssociationMapping('person')['joinColumns'][0]['name'];
        $participationToRole = $participationMtd->getAssociationMapping('role')['joinColumns'][0]['name'];
        $participationToStatus = $participationMtd->getAssociationMapping('status')['joinColumns'][0]['name'];
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
        
        $sql = <<<EOT
WITH qualification_on_participation AS (SELECT 
	participation.$participationToPerson AS person_id, 
        get_last_qualification_edd(participation.person_id, event.date) AS qualification
FROM 	$participationTable AS participation 
JOIN 	$eventTable AS event ON participation.$participationToEvent = event.$eventId
WHERE 
	-- event_date
	event.$eventDate BETWEEN :date_from::date AND :date_to::date
	AND
        -- event centers
        event.$eventToCenter IN (:authorizedCenters)
        AND
	-- event type pour "école de devoir"
	event.$eventToType IN (:selectedEventTypes)
	AND
	-- participation status
	participation.$participationToStatus IN (:selectedParticipationStatus)
	AND
        --role for animators
        participation.$participationToRole IN (:animatorRoles)
)
SELECT DISTINCT
	person.$personLastname || ' ' || person.$personFirstname AS name,
	EXTRACT(YEAR from person.$personBirthdate) AS year_of_birthdate,
	CASE WHEN qualification_on_participation.qualification LIKE 'brevet' THEN 'x' ELSE '' END as brevet,
        CASE WHEN qualification_on_participation.qualification LIKE 'assimil' THEN 'x' ELSE '' END as assimil,
	CASE WHEN qualification_on_participation.qualification LIKE 'equival' THEN 'x' ELSE '' END as equival,
	CASE WHEN (
		qualification_on_participation.qualification IS NULL 
		OR 
		qualification_on_participation.qualification NOT IN ('brevet', 'equival', 'assimil')
		) THEN 'x' ELSE '' END as non_qualified
FROM qualification_on_participation
JOIN $personTable AS person ON person.$personId = qualification_on_participation.person_id
ORDER BY name
;
EOT;
        $rsm = (new ResultSetMappingBuilder($this->entityManager))
                ->addScalarResult('name', 'name')
                ->addScalarResult('year_of_birthdate', 'year_of_birthdate')
                ->addScalarResult('brevet', 'brevet')
                ->addScalarResult('assimil', 'assimil')
                ->addScalarResult('equival', 'equival')
                ->addScalarResult('non_qualified', 'non_qualified')
                ;
        
        return $this->entityManager->createNativeQuery($sql, $rsm)
            ->setParameter('selectedEventTypes', $eventTypesIds)
            ->setParameter('authorizedCenters', $authorizedCentersIds)
            ->setParameter('animatorRoles', $roleAnimator)
            ->setParameter('date_from', $dateFrom, Type::DATE)
            ->setParameter('date_to', $dateTo, Type::DATE)
            ->setParameter('selectedParticipationStatus', $statusesIds)
            ;
    }
    

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }

    
}
