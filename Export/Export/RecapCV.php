<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Export\Export;

namespace Chill\ONEStatBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\ONEStatBundle\Form\ONEExportPreparationType;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Chill\EventBundle\Entity\Role as EventRole;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;
use Doctrine\DBAL\Types\Type;

/**
 * Provide the "Tableau des présences journalières" required by Annexe III.
 * 
 * The results are pivoted to make dates appears on first row.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class RecapCV implements ExportInterface
{    
        
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     * The keys used in the query
     *
     * @var string[]
     */
    protected $queryKeys = array(
        'event_date' => "Date",
        'nb_childs' => "Nombre d'enfants",
        'nb_animator_paid_qualified' => "Nombre d'animateurs qualifiés indemnisés",
        'nb_animator_volunteer_qualified' => "Nombre d'animateurs qualifiés non indemnisés",
        'nb_animator_stage' => "Nombre d'animateurs en 2ième année de stage",
        'nb_animator_not_qualified' => "Nombre d'animateurs non qualifiés",
        'nb_animator' => "Nombre total d'animateurs"
    );
    

    public function __construct(
                EntityManagerInterface $em
            )
    {
        $this->entityManager = $em;
    }
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('one', ONEExportPreparationType::class, array(
           'ONE_type' => 'plaine', 
           'animator' => true,
           'child' => true
        ));  
    }

    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_LIST);
    }

    public function getDescription()
    {
        return "Fournit le \"tableau de présences journalière\" pour les Centres"
                . " de vacances. (Annexe III). Les tableaux pour les enfants de "
                . "30 mois à 5 ans, et celui pour les enfants de 6 à 15 ans, doit "
                . "être produit séparément.";
    }

    public function getLabels($key, array $values, $data)
    {
        switch ($key) {
            case 'event_date' :
                return function ($v) { 
                    return $v === '_header' ? 
                        $this->queryKeys['event_date'] : 
                        $v->format('d-m-Y');
                };
            default:
                return function ($v) use ($key) { 
                    return $v === '_header' ?
                            $this->queryKeys[$key] : (is_null($v) ? 0 : $v); 
                };
        }
    }

    public function getQueryKeys($data)
    {
        return array_keys($this->queryKeys);
    }

    /**
     * 
     * 
     * @param \Doctrine\ORM\NativeQuery $query
     * @param array $data
     */
    public function getResult($query, $data)
    {
        return $query->getResult();
    }

    public function getTitle()
    {
        return "Tableau de présences journalières (Annexe III)";
    }

    public function getType()
    {
        return 'participation';
    }

    public function initiateQuery(array $requiredModifiers, array $acl, array $data = array())
    {
        // get the parameters for the query
        $dateFrom = $data['one']['date_from'];
        $dateTo = $data['one']['date_to'];
        $birthdateFrom =    $data['one']['child']['age']['birth_from'];
        $birthdateTo =      $data['one']['child']['age']['birth_to'];
        $roleChildIds = array_map(
                function(EventRole $r) { return $r->getId(); }, 
                $data['one']['child']['roles']->toArray()
                );
        $roleAnimatorVolunteersIds = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles_volunteers']->toArray()
                );
        $roleAnimatorPaidIds = array_map(
                function (EventRole $r) { return $r->getId(); },
                $data['one']['animator']['roles_paid']->toArray()
                );
        $eventTypesIds = array_map(
                function (EventType $t) { return $t->getId(); },
                $data['one']['type']->toArray()
                );
        $statusesIds = array_map(
                function (Status $s) { return $s->getId(); },
                $data['one']['statuses']->toArray()
                );
        $authorizedCentersIds = array_map(
            function ($el) { return $el['center']->getId(); },
            $acl
            );
        
        // store table string in string
        $eventMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Event');
        $participationMtd = $this->entityManager->getClassMetadata('ChillEventBundle:Participation');
        $qualificationMtd = $this->entityManager->getClassMetaData('ChillONEStatBundle:Qualification');
        $personMtd = $this->entityManager->getClassMetadata('ChillPersonBundle:Person');

        // table name
        $participationTable = $participationMtd->getTableName();
        $eventTable = $eventMtd->getTableName();
        $personTable = $personMtd->getTableName();
        
        // field
        $eventDate = $eventMtd->getColumnName('date');
        $eventId = $eventMtd->getColumnName('id');
        $personId = $personMtd->getColumnName('id');
        $participationId = $participationMtd->getColumnName('id');
        
        // joins between table
        $participationToEvent = $participationMtd->getAssociationMapping('event')['joinColumns'][0]['name'];
        $participationToPerson = $participationMtd->getAssociationMapping('person')['joinColumns'][0]['name'];
        $participationToRole = $participationMtd->getAssociationMapping('role')['joinColumns'][0]['name'];
        $participationToStatus = $participationMtd->getAssociationMapping('status')['joinColumns'][0]['name'];
        $eventToType = $eventMtd->getAssociationMapping('type')['joinColumns'][0]['name'];
        $eventToCenter = $eventMtd->getAssociationMapping('center')['joinColumns'][0]['name'];
        
        $sql = <<<EOT
WITH participants_list AS (SELECT 
	participation.$participationId AS id, 
        participation.$participationToPerson AS person_id,
        participation.$participationToRole AS role_id,
        event.$eventDate AS event_date,
        get_last_qualification_cv(participation.$participationToPerson, event.$eventDate) AS qualification
FROM 	$participationTable AS participation 
JOIN 	$eventTable AS event ON participation.$participationToEvent = event.$eventId
WHERE 
	-- event_date
	event.$eventDate BETWEEN :date_from::date AND :date_to::date
	AND
        -- event centers
        event.$eventToCenter IN (:centers)
        AND
	-- event type pour "centre de vacances"
	event.$eventToType IN (:event_types)
	AND
	-- participation status
	participation.$participationToStatus IN (:statuses)
	AND
	(
		--role for volunteers
		participation.$participationToRole IN (:role_animator_volunteer)
		OR
		--role for "indemnisé"
		participation.$participationToRole IN (:role_animator_paid)
                OR
		-- role for child
		participation.$participationToRole IN (:role_child)
	)
)
SELECT DISTINCT
	list_dates.event_date,
	child_list.nb_childs AS nb_childs,
	animator_paid_qualified.nb_animator AS nb_animator_paid_qualified,
	animator_volunteer_qualified.nb_animator AS nb_animator_volunteer_qualified,
	animator_stage.nb_animator AS nb_animator_stage,
	animator_not_qualified.nb_animator AS nb_animator_not_qualified,
	animator_sum.nb_animator AS nb_animator
FROM ( 
	SELECT 
		generate_series(:date_from::date, :date_to::date, '1 day'::interval)::date AS event_date
	)  list_dates
LEFT JOIN (
	--query for "animateur qualifie indemnisé"
	SELECT participants_list.event_date, count(person_id) AS nb_animator FROM participants_list 
	WHERE 
		-- restrict to animators paid
		participants_list.role_id IN (:role_animator_paid)
		AND
		-- retrict to qualification
		participants_list.qualification IN ('brevet', 'assimil', 'equival')
	GROUP BY event_date
) AS animator_paid_qualified ON list_dates.event_date = animator_paid_qualified.event_date
LEFT JOIN (
	--query for "animator qualifié non indemnisé"
	SELECT participants_list.event_date, count(person_id) AS nb_animator FROM participants_list 
	WHERE
		-- restrict to animators volunteers
		participants_list.role_id IN (:role_animator_volunteer)
		AND
		participants_list.qualification IN ('brevet', 'assimil', 'equival')
	GROUP BY event_date
) AS animator_volunteer_qualified ON list_dates.event_date = animator_volunteer_qualified.event_date
LEFT JOIN (
	--query for "animator en deuxième année de stage"
	SELECT participants_list.event_date, count(person_id) AS nb_animator FROM participants_list 
	WHERE
		-- restrict to animators (volunteers or paid)
		(
			participants_list.role_id IN (:role_animator_volunteer) 
			OR
			participants_list.role_id IN (:role_animator_paid)
		)
		AND
		participants_list.qualification IN ('stage')
	GROUP BY event_date
) AS animator_stage ON list_dates.event_date = animator_stage.event_date
LEFT JOIN (
	--query for "animator non qualifié"
	SELECT participants_list.event_date, count(person_id) AS nb_animator FROM participants_list 
	WHERE
		-- restrict to animators (volunteers or paid)
		(
			participants_list.role_id IN (:role_animator_volunteer) 
			OR
			participants_list.role_id IN (:role_animator_paid)
		) 
		AND
		(
                    participants_list.qualification NOT IN ('brevet', 'assimil', 'equival')
                    OR
                    participants_list.qualification IS NULL
                )
	GROUP BY event_date
) AS animator_not_qualified ON list_dates.event_date = animator_not_qualified.event_date
LEFT JOIN (
	--query for "nombre total d'animateur"
	SELECT participants_list.event_date, count(person_id) AS nb_animator FROM participants_list 
	WHERE 
		-- restrict to animators (volunteers or paid)
		(
			participants_list.role_id IN (:role_animator_volunteer) 
			OR
			participants_list.role_id IN (:role_animator_paid)
		) 
	GROUP BY event_date
) AS animator_sum ON list_dates.event_date = animator_sum.event_date
LEFT JOIN (
	-- query for participants / childs
	SELECT participants_list.event_date, count(person_id) AS nb_childs 
        FROM participants_list
        JOIN $personTable as person ON person.$personId = participants_list.$participationToPerson
	WHERE
		-- restrict to childs
		participants_list.role_id IN (:role_child)
                AND
                person.birthdate BETWEEN :birthdate_from::date AND :birthdate_to::date
	GROUP BY event_date
) AS child_list ON child_list.event_date = list_dates.event_date
ORDER BY list_dates.event_date

EOT;
        $rsm = (new ResultSetMappingBuilder($this->entityManager))
                ->addScalarResult('event_date', 'event_date', Type::DATE)
                ->addScalarResult('nb_childs', 'nb_childs')
                ->addScalarResult('nb_animator_paid_qualified', 'nb_animator_paid_qualified')
                ->addScalarResult('nb_animator_volunteer_qualified', 'nb_animator_volunteer_qualified')
                ->addScalarResult('nb_animator_stage', 'nb_animator_stage')
                ->addScalarResult('nb_animator_not_qualified', 'nb_animator_not_qualified')
                ->addScalarResult('nb_animator', 'nb_animator');
        
        return $this->entityManager->createNativeQuery($sql, $rsm)
                ->setParameter('event_types', $eventTypesIds)
                ->setParameter('role_animator_volunteer', $roleAnimatorVolunteersIds)
                ->setParameter('role_animator_paid', $roleAnimatorPaidIds)
                ->setParameter('role_child', $roleChildIds)
                ->setParameter('date_from', $dateFrom, Type::DATE)
                ->setParameter('date_to', $dateTo, Type::DATE)
                ->setParameter('centers', $authorizedCentersIds)
                ->setParameter('statuses', $statusesIds)
                ->setParameter('birthdate_from', $birthdateFrom)
                ->setParameter('birthdate_to', $birthdateTo)
                ;
        
    }

    public function requiredRole()
    {
        return new Role(ONEStatsVoter::STATS);
    }

    public function supportsModifiers()
    {
        return array();
    }

}
