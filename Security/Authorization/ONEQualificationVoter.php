<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\ONEStatBundle\Entity\Qualification;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ONEQualificationVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{

    const UPDATE  = 'CHILL_ONESTATS_QUALIFICATION_UPDATE';
    const CREATE  = 'CHILL_ONESTATS_QUALIFICATION_CREATE';
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return array(self::UPDATE, self::CREATE);
    }

    protected function getSupportedClasses()
    {
        return array(Qualification::class);
    }

    protected function isGranted($attribute, $object, $user = null)
    {
        if (!$user instanceof User) {
            return false;
        }
        
        if (!$object instanceof Qualification) {
            throw new \LogicException("the expected type is ".Qualification::class." for "
                    . "role ".$attribute." ".get_class($object)." given.");
        }
        
        return $this->helper->userHasAccess($user, $object, $attribute);
        
    }

    public function getRoles()
    {
        return $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return $this->getSupportedAttributes();
    }
    
    public function getRolesWithHierarchy()
    {
        return [ 'ONE' => $this->getRoles() ];
    }
}
