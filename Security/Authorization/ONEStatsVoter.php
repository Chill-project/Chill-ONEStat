<?php

/*
 * Copyright (C) 2015 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\MainBundle\Entity\Center;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ONEStatsVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{

    const STATS  = 'CHILL_ONESTATS_STATS';
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return array(self::STATS);
    }

    protected function getSupportedClasses()
    {
        return array(Center::class);
    }

    protected function isGranted($attribute, $object, $user = null)
    {
        if (!$user instanceof User) {
            return false;
        }
        
        if ($attribute !== self::STATS and !$object instanceof \Chill\PersonBundle\Entity\Person) {
            throw new \LogicException("the expected type is \Chill\PersonBundle\Entity\Person for "
                    . "role ".$attribute." ".get_class($object)." given.");
        }
        
        return $this->helper->userHasAccess($user, $object, $attribute);
        
    }

    public function getRoles()
    {
        return $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return $this->getSupportedAttributes();
    }
    
    public function getRolesWithHierarchy()
    {
        return [ 'ONE' => $this->getRoles() ];
    }
}
