<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\ONEStatBundle\Entity\Qualification;
use Chill\ONEStatBundle\Security\Authorization\ONEQualificationVoter;
use Chill\ONEStatBundle\Form\QualificationType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class QualificationController extends Controller
{
    public function listAction($person_id)
    {
        $person = $this->getDoctrine()->getManager()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException('person not found');
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, 
                "you are not allowed to see this person");
        
        $qualifications = $this->getDoctrine()->getManager()
                ->createQuery('SELECT q FROM ChillONEStatBundle:Qualification q '
                        . 'WHERE q.person = :person '
                        . 'ORDER BY q.since DESC ')
                ->setParameter('person', $person)
                ->getResult();
        
        return $this->render('ChillONEStatBundle:Qualification:list.html.twig', array(
                'person' => $person,
                'qualifications' => $qualifications,
                'empty_qualification' => (new Qualification())->setPerson($person)
            ));    
        
    }
    
    public function newAction($person_id, Request $request)
    {
        $person = $this->getDoctrine()->getManager()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException('person not found');
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, 
                "you are not allowed to see this person");
        
        $qualification = (new Qualification())
                ->setPerson($person);
        
        $this->denyAccessUnlessGranted(ONEQualificationVoter::CREATE, $qualification, 
                "you are not allowed to create a qualification");
        
        $form = $this->createCreateForm($qualification);
        
        return $this->render('ChillONEStatBundle:Qualification:new.html.twig', array(
            'person' => $person,
            'qualification' => $qualification,
            'form' => $form->createView()
        ));
    }
    
    public function createAction($person_id, Request $request)
    {
        $person = $this->getDoctrine()->getManager()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException('person not found');
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, 
                "you are not allowed to see this person");
        
        $qualification = (new Qualification())
                ->setPerson($person);
        
        $this->denyAccessUnlessGranted(ONEQualificationVoter::CREATE, $qualification, 
                "you are not allowed to create a qualification");
        
        $form = $this->createCreateForm($qualification);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($qualification);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans("The qualification was successfully created"));
            
            return $this->redirectToRoute('chill_onestat_qualification_list', array(
                'person_id' => $qualification->getPerson()->getId()
            ));
        }
        
        return $this->render('ChillONEStatBundle:Qualification:new.html.twig', array(
            'person' => $person,
            'qualification' => $qualification,
            'form' => $form->createView()
        ));
    }
    
    /**
     * 
     * @param Qualification $qualification
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createCreateForm(Qualification $qualification)
    {
        $form = $this->createForm(QualificationType::class, $qualification, array(
            'action' => $this->generateUrl('chill_onestat_qualification_create', array(
                'person_id' => $qualification->getPerson()->getId()
            ))
        ));
        
        $form->add('submit', SubmitType::class, array(
            'label' => 'Create'
        ));
        
        return $form;
    }
    
    public function createEditForm(Qualification $qualification)
    {
        $form = $this->createForm(QualificationType::class, $qualification, array(
            'action' => $this->generateUrl('chill_onestat_qualification_update', array(
                'person_id' => $qualification->getPerson()->getId(),
                'qualification_id' => $qualification->getId()
            ))
        ));
        
        $form->add('submit', SubmitType::class, array(
            'label' => 'Update'
        ));
        
        return $form;
    }

    public function editAction($person_id, $qualification_id)
    {
        $person = $this->getDoctrine()->getManager()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException('person not found');
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, 
                "you are not allowed to see this person");
        
        $qualification = $this->getDoctrine()->getManager()
                ->getRepository('ChillONEStatBundle:Qualification')
                ->find($qualification_id);
        
        if ($qualification === null) {
            throw $this->createNotFoundException('qualification not found');
        }
        
        if ($qualification->getPerson()->getId() !== $person->getId()) {
            throw $this->createNotFoundException('qualification does not match person');
        }
        
        $this->denyAccessUnlessGranted(ONEQualificationVoter::UPDATE, $qualification, 
                "you are not allowed to create a qualification");
        
        $form = $this->createEditForm($qualification);
        
        return $this->render('ChillONEStatBundle:Qualification:edit.html.twig', array(
            'person' => $person,
            'qualification' => $qualification,
            'form' => $form->createView()
        ));    
        
    }
    
    public function updateAction($person_id, $qualification_id, Request $request)
    {
        $person = $this->getDoctrine()->getManager()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException('person not found');
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, 
                "you are not allowed to see this person");
        
         $qualification = $this->getDoctrine()->getManager()
                ->getRepository('ChillONEStatBundle:Qualification')
                ->find($qualification_id);
        
        if ($qualification === null) {
            throw $this->createNotFoundException('qualification not found');
        }
        
        if ($qualification->getPerson()->getId() !== $person->getId()) {
            throw $this->createNotFoundException('qualification does not match person');
        }
        
        $this->denyAccessUnlessGranted(ONEQualificationVoter::UPDATE, $qualification, 
                "you are not allowed to create a qualification");
        
        $form = $this->createEditForm($qualification);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()
                    ->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans("The qualification was updated"));
            
            return $this->redirectToRoute('chill_onestat_qualification_list', array(
                'person_id' => $qualification->getPerson()->getId()
            ));
        }
        
        return $this->render('ChillONEStatBundle:Qualification:edit.html.twig', array(
            'person' => $person,
            'qualification' => $qualification,
            'form' => $form->createView()
        ));    
        
    }

}
