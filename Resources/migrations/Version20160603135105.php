<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add function to get the last qualification by person and date.
 */
class Version20160603135105 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE OR REPLACE FUNCTION get_last_qualification_cv (pid integer, before_date date) RETURNS VARCHAR(9)
AS $$ 
SELECT qualification 
FROM (
	SELECT qualification, rank() OVER (PARTITION BY person_id ORDER BY since DESC) as pos
	FROM chill_onestat_qualification AS qualification
	WHERE 
		cv IS TRUE
		AND
		qualification.person_id = pid
		AND
		qualification.since <= before_date
) AS qualification_ordered WHERE qualification_ordered.pos = 1  $$ LANGUAGE SQL;");
        
        $this->addSql("CREATE OR REPLACE FUNCTION get_last_qualification_edd (pid integer, before_date date) RETURNS VARCHAR(9)
AS $$ 
SELECT qualification 
FROM (
	SELECT qualification, rank() OVER (PARTITION BY person_id ORDER BY since DESC) as pos
	FROM chill_onestat_qualification AS qualification
	WHERE 
		edd IS TRUE
		AND
		qualification.person_id = pid
		AND
		qualification.since <= before_date
) AS qualification_ordered WHERE qualification_ordered.pos = 1  $$ LANGUAGE SQL;");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP FUNCTION get_last_qualification_cv(integer, date)");
        $this->addSql("DROP FUNCTION get_last_qualification_edd(integer, date)");
    }
}
