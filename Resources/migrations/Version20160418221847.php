<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adding the Animator table
 */
class Version20160418221847 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_onestat_qualification_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_onestat_qualification 
            (id INT NOT NULL, 
            person_id INT DEFAULT NULL,  
            qualification VARCHAR(9) NOT NULL, 
            since DATE NOT NULL, 
            cv BOOLEAN NOT NULL,
            edd BOOLEAN NOT NULL,
            PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4865763D217BBB47 ON chill_onestat_qualification (person_id)');
        $this->addSql('ALTER TABLE chill_onestat_qualification ADD CONSTRAINT FK_4865763D217BBB47 FOREIGN KEY (person_id) '
                . 'REFERENCES '.$this->getPersonTableName().' (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        // add a constraint to make the table unique
        $this->addSql('ALTER TABLE chill_onestat_qualification '
                . 'ADD CONSTRAINT chill_onestat_qualification_unique_person_date '
                . 'UNIQUE (person_id, since)');
        // add an index on person_id and since for easying querying last qualification for a person
        $this->addSql('CREATE INDEX chill_onestat_qualification_person_since '
                . 'ON chill_onestat_qualification USING btree (person_id, since);');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE chill_onestat_qualification_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_onestat_qualification');
    }
    
    /**
     * This function return the name of the person table.
     * 
     * The person table was renamed on 
     * https://framagit.org/Chill-project/Chill-Person/commit/52fb5f56be623acd605134b90392040f98a5359c
     * 
     * We guess here the person_table name.
     */
    private function getPersonTableName()
    {
        $column = $this->connection->fetchColumn("select to_regclass('person')");
        
        return $column === null ? 'chill_person_person' : 'person';
    }
}
