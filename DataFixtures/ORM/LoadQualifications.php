<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\ONEStatBundle\Entity\Qualification;
use Chill\MainBundle\DataFixtures\ORM\LoadCenters;

/**
 * Add 20 animators (= person) with qualifications
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadQualifications extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     *
     * @var \Faker
     */
    protected $faker;
    
    public static $refs_animator = array();
    
    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_BE');
    }

    public function getOrder()
    {
        return 35012;
    }

    /**
     * Add new people and qualifications
     * 
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $genders = array(Person::FEMALE_GENDER, Person::MALE_GENDER);
        $qualifications = array(
            Qualification::ASSIMILATED,
            Qualification::BREVET,
            Qualification::EQUIVALENT,
            Qualification::NO_QUALIF
        );
        
        for ($i=0; $i <= 30; $i++) {
            // creating an animator
            $gender = $genders[array_rand($genders)];
            $animator = (new Person())
                    ->setBirthdate($this->faker->dateTimeBetween('-25 years', '-16 years'))
                    ->setPhonenumber($this->faker->phoneNumber)
                    ->setEmail($this->faker->email)
                    ->setFirstName( $gender === Person::FEMALE_GENDER ? 
                            $this->faker->firstNameFemale : $this->faker->firstNameMale
                            )
                    ->setLastName($this->faker->lastName.' animator')
                    ->setGender($gender)
                    ->setCenter($this->getReference(
                            LoadCenters::$refs[array_rand(LoadCenters::$refs)]
                            ));
                    ;
            $this->addReference('animator_'.$i, $animator);
            static::$refs_animator[] = 'animator_'.$i;
            
            $manager->persist($animator);
            
            // adding some qualifications
            for ($q=0; $q <= rand(0, 3); $q++) {
                $qualification = (new Qualification)
                        ->setQualification($qualifications[array_rand($qualifications)])
                        ->setSince($this->faker->dateTimeBetween('-4 years', '- 2 months'))
                        ->setPerson($animator)
                        ->setCv((boolean) rand(0,1))
                        ->setEdd((boolean) rand(0,1))
                        ;
                $manager->persist($qualification);
            }
            
        }
        
        $manager->flush();
    }

}
