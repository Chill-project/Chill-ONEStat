<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\ONEStatBundle\DataFixtures\ORM\LoadQualifications;
use Chill\MainBundle\DataFixtures\ORM\LoadCenters;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Role;
use Chill\EventBundle\Entity\Participation;
use Chill\EventBundle\Entity\Status;
use Chill\EventBundle\Entity\Event;
use Chill\MainBundle\Entity\Center;
use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadEvents extends AbstractFixture implements OrderedFixtureInterface
{
    protected $events = array();
    
    /**
     *
     * @var \Faker
     */
    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_BE');
    }
    
    public function getOrder()
    {
        return 35020;
    }

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $this->loadDefinitions($manager);
        $this->loadEventsEDD($manager);
        $this->loadEventsCV($manager);
        $this->loadParticipations($manager);
        
        $manager->flush();
    }
    
    public function loadDefinitions(ObjectManager $manager)
    {
        // adding "Ecole de devoirs"
        $type = (new EventType())
                ->setActive(true)
                ->setName(array('fr' => 'Ecole de devoir'));
        $manager->persist($type);
        $this->addReference('event_type_ecole_devoir', $type);
        
        $status = (new Status())
                ->setActive(true)
                ->setName(array('fr' => 'Présent'))
                ->setType($type)
                ;
        $manager->persist($status);
        $this->addReference('event_status_present_devoir', $status);
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Animateur'))
                ->setType($type);
        $manager->persist($role);
        $this->addReference('event_role_animateur_devoir', $role);
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Enfant'))
                ->setType($type);
        $manager->persist($role);
        $this->addReference('event_role_enfant_devoir', $role);
        
        // adding "Centre de vacances"
        $type = (new EventType())
                ->setActive(true)
                ->setName(array('fr' => 'Centre de vacances'));
        $manager->persist($type);
        $this->addReference('event_type_cv', $type);
        
        $status = (new Status())
                ->setActive(true)
                ->setName(array('fr' => 'Présent'))
                ->setType($type)
                ;
        $manager->persist($status);
        $this->addReference('event_status_present_cv', $status);
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Animateur indemnisé'))
                ->setType($type);
        $manager->persist($role);
        $this->addReference('event_role_animateur_cv_paid', $role);
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Animateur bénévole'))
                ->setType($type);
        $manager->persist($role);
        $this->addReference('event_role_animateur_cv_volunteers', $role);
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Enfant'))
                ->setType($type);
        $manager->persist($role);
        $this->addReference('event_role_enfant_cv', $role);
    }
    
    protected function loadEventsEDD(ObjectManager $manager)
    {
        $date = \DateTime::createFromFormat('Y-m-d', '2016-04-11');
        $centers = array_map(
              function ($ref) { return $this->getReference($ref); },
              LoadCenters::$refs
              );
        
        for ($i=0; $i <= 30; $i++) {
            $date = $this->getNextOpenDay($date);
            
            foreach($centers as $center) {
                $event = (new Event())
                    ->setDate($date)
                    ->setCenter($center)
                    ->setName(sprintf("Ecole de devoir du %s", $date->format('d-m-Y')))
                    ->setType($this->getReference('event_type_ecole_devoir'))
                    ->setCircle($this->getReference('scope_all'))
                        ;
                $manager->persist($event);
                $this->events['edd'][$center->getId()][] = $event;
                
                // adding some animators
                $max = rand(1, 5);
                // reset the excluding animator id
                $this->excludingAnimatorId = array();
                for ($j=0; $j <= $max; $j++) {
                    $participation = (new Participation())
                            ->setPerson($this->getAnimator($center))
                            ->setRole($this->getReference('event_role_animateur_devoir'))
                            ->setStatus($this->getReference('event_status_present_devoir'))
                            ->setEvent($event)
                            ;
                    $manager->persist($participation);
                }
                
            }
        }
    }
    
    protected function loadEventsCV(ObjectManager $manager)
    {
        $date = \DateTime::createFromFormat('Y-m-d', '2016-07-01');
        $centers = array_map(
              function ($ref) { return $this->getReference($ref); },
              LoadCenters::$refs
              );
        
        for ($i=0; $i <= 60; $i++) {
            $date = $this->getNextOpenDay($date);
            
            foreach($centers as $center) {
                $event = (new Event())
                    ->setDate($date)
                    ->setCenter($center)
                    ->setName(sprintf("Journée de centre de vacances du %s", $date->format('d-m-Y')))
                    ->setType($this->getReference('event_type_cv'))
                    ->setCircle($this->getReference('scope_all'))
                        ;
                $manager->persist($event);
                $this->events['cv'][$center->getId()][] = $event;
                
                // adding some animators
                $max = rand(1, 5);
                // reset the excluding animator id
                $this->excludingAnimatorId = array();
                for ($j=0; $j <= $max; $j++) {
                    // paid or volunteers ?
                    $roleReference = rand(0,1) === 0 ?
                          'event_role_animateur_cv_paid' :
                          'event_role_animateur_cv_volunteers';
                    
                    $participation = (new Participation())
                            ->setPerson($this->getAnimator($center))
                            ->setRole($this->getReference($roleReference))
                            ->setStatus($this->getReference('event_status_present_cv'))
                            ->setEvent($event)
                            ;
                    $manager->persist($participation);
                }
                
            }
        }
    }
    
    private function getNextOpenDay(\DateTime $date)
    {
        $date = clone $date;
        $day = $date->add(new \DateInterval('P1D'));
        
        if (in_array($day->format('D'), array('Sat', 'Sun'))) {
            return $this->getNextOpenDay($day);
        }
        
        return $day;
    }
    
    private $excludingAnimatorId = array();
    
    private function getAnimator(Center $center)
    {
        /* @var $animator \Chill\PersonBundle\Entity\Person */
        $animator = $this->getReference(
                LoadQualifications::$refs_animator[
                    array_rand(LoadQualifications::$refs_animator)
                    ]
                );
        
        if ($animator->getCenter()->getId() !== $center->getId()) {
            return $this->getAnimator($center);
        }
        
        // check if an animator has already been added
        if (in_array($animator->getId(), $this->excludingAnimatorId)) {
            return $this->getAnimator($center);
        }
        
        // add the animator to the list of excluded for future execution
        $this->excludingAnimatorId[] = $animator->getId();
        
        return $animator;
    }
    
    /**
     * Load participations
     * 
     * The first 5 event will receive the participation of all recorded
     * children, the following will receive a randomly number of participations.
     * 
     * @param ObjectManager $manager
     */
    protected function loadParticipations(ObjectManager $manager)
    {        
        foreach (LoadChildren::$refs as $ref) {

            $child = $this->getReference($ref);
            
            $manager->persist($child);
            
            // adding some participation to école de devoir (EDD)
            $nb = rand(5, 30); // determine how many events after the event #5 a participation will be added
            for ($j=0; $j <= $nb; $j++) {
                $event = $this->events['edd'][$child->getCenter()->getId()][$j];
                
                $participation = (new Participation())
                    ->setEvent($event)
                    ->setRole($this->getReference('event_role_enfant_devoir'))
                    ->setPerson($child)
                    ->setStatus($this->getReference('event_status_present_devoir'))
                        ;
                $manager->persist($participation);
            }
            
            // adding some participation to Centre de vacances
            $nb = rand(5, 60); // determine how many events after the event #5 a participation will be added
            for ($j=0; $j <= $nb; $j++) {
                $event = $this->events['cv'][$child->getCenter()->getId()][$j];
                
                $participation = (new Participation())
                    ->setEvent($event)
                    ->setRole($this->getReference('event_role_enfant_cv'))
                    ->setPerson($child)
                    ->setStatus($this->getReference('event_status_present_cv'))
                        ;
                $manager->persist($participation);
            }
        }
    }

}
