<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace Chill\ONEStatBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\ONEStatBundle\Security\Authorization\ONEStatsVoter;
use Chill\ONEStatBundle\Security\Authorization\ONEQualificationVoter;

/**
 * Add roles to existing groups
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LoadRolesACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            /* @var $permissionsGroup \Chill\MainBundle\Entity\PermissionsGroup */
            $permissionsGroup = $this->getReference($permissionsGroupRef);
            \printf(
                    "Adding %s to permission group %s\n",
                    ONEStatsVoter::STATS." ".ONEQualificationVoter::CREATE." ".ONEQualificationVoter::UPDATE,
                    $permissionsGroup->getName()
                    );
            
            $roleScope = (new RoleScope())
                    ->setRole(ONEStatsVoter::STATS)
                    ;
            $permissionsGroup->addRoleScope($roleScope);
            $manager->persist($roleScope);
            
            $roleScope = (new RoleScope())
                    ->setRole(ONEQualificationVoter::CREATE)
                    ;
            $permissionsGroup->addRoleScope($roleScope);
            $manager->persist($roleScope);
            
            $roleScope = (new RoleScope())
                    ->setRole(ONEQualificationVoter::UPDATE);
            $permissionsGroup->addRoleScope($roleScope);
            $manager->persist($roleScope);
        }
        
        $manager->flush();
    }

    public function getOrder()
    {
        return 35011;
    }

}
