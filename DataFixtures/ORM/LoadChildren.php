<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\MainBundle\DataFixtures\ORM\LoadCenters;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Chill\CustomFieldsBundle\Entity\CustomFieldLongChoice\Option;
use Chill\MainBundle\DataFixtures\ORM\LoadPostalCodes;
use Chill\MainBundle\Entity\Address;

/**
 * Load a set of childs
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LoadChildren extends AbstractFixture implements 
    OrderedFixtureInterface, 
    ContainerAwareInterface
{
    
    protected $faker;
    
    public static $refs = array();
    
    CONST MAX_CHILDREN = 30;
    CONST MAX_SCHOOL = 15;
    CONST SCHOOL_CF_SLUG = 'school-2fb5440e-192c-11e6-b2fd-74d02b0c9b55';
    const SCHOOL_CF_REF = 'school_custom_field';
    
    use ContainerAwareTrait;
    
    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_BE');
    }
    
    public function getOrder()
    {
        return 35016;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadCustomFieldSchool($manager);
        $this->loadSchools($manager);
        $this->loadChilds($manager);
        $manager->flush();
    }
    
    protected function loadCustomFieldSchool(ObjectManager $manager)
    {
        $cfGroup = new \Chill\CustomFieldsBundle\Entity\CustomFieldsGroup();
        $cfGroup->setName(array('en' => "scholarship", 'nl' => 'schools', 'fr' => 'scolarité'))
              ->setEntity(Person::class)
              ;
        $manager->persist($cfGroup);
        
        $cf = (new \Chill\CustomFieldsBundle\Entity\CustomField())
              ->setActive(true)
              ->setCustomFieldsGroup($cfGroup)
              ->setName(array('fr' => 'école', 'nl' => 'school', 'en' => 'school'))
              ->setOrdering(10)
              ->setSlug(self::SCHOOL_CF_SLUG)
              ->setType("long_choice")
              ->setOptions(array(
                 'key' => 'schools'
              ));
        $manager->persist($cf);
        $this->setReference('school_custom_field', $cf);
    }
    
    protected function loadSchools(ObjectManager $manager)
    {
        $parent = (new Option())
              ->setActive(true)
              ->setKey('schools')
              ->setInternalKey($this->faker->uuid)
              ->setText(array('fr' => 'Écoles', 'nl' => 'Scholen', 'en' => 'Schools'))
              ;
        $manager->persist($parent);
        
        for ($i=0; $i<=15; $i++) {
            $name = 'École '.$this->faker->name;
            $school = (new Option())
                  ->setActive(true)
                  ->setKey('schools')
                  ->setInternalKey($this->faker->uuid)
                  ->setText(array('fr' => $name, 'nl' => $name, 'en' => $name))
                  ->setParent($parent)
                  ;
            $manager->persist($school);
            $this->setReference('school_'.$i, $school);
        }
    }

    protected function loadChilds(ObjectManager $manager)
    {
        $genders = array(Person::FEMALE_GENDER, Person::MALE_GENDER);
        
        for ($i=0; $i<= self::MAX_CHILDREN; $i++) {
            // creating a children
            $gender = $genders[array_rand($genders)];
            $child = (new Person($this->faker->dateTimeBetween('-5 years', '-3 years')))
                    ->setBirthdate($this->faker->dateTimeBetween('-16 years', '-5 years'))
                    ->setPhonenumber($this->faker->phoneNumber)
                    ->setEmail($this->faker->email)
                    ->setFirstName( $gender === Person::FEMALE_GENDER ? 
                            $this->faker->firstNameFemale : $this->faker->firstNameMale
                            )
                    ->setLastName($this->faker->lastName)
                    ->setGender($gender)
                    ->setCenter($this->getReference(
                            LoadCenters::$refs[array_rand(LoadCenters::$refs)]
                            ));
                    ;
            // add a school to 80% of those childs
            if (rand(1,10) < 8) {
                $school = $this->getReference('school_'.rand(0, self::MAX_SCHOOL));
                $schoolRepresentation = 
                $this->container
                      ->get('chill.custom_field.provider')
                      ->getCustomFieldByType('long_choice')
                      ->serialize($school, $this->getReference(self::SCHOOL_CF_REF));
                
                $child->setCFData(array(
                   self::SCHOOL_CF_SLUG => $schoolRepresentation
                ));
            }
            
            // add an address to 90% of those childs
            if (rand(1, 10) < 9) {
                $address = (new Address())
                      ->setStreetAddress1($this->faker->streetAddress)
                      ->setStreetAddress2(rand(0,3) === 3 ? $this->faker->streetAddress : '')
                      ->setPostcode(
                            $this->getReference(
                                  LoadPostalCodes::$refs[array_rand(LoadPostalCodes::$refs)]
                                  )
                            )
                      ->setValidFrom($this->faker->dateTimeBetween('-5 years', 'now'));
                $child->addAddress($address);
            }
            
            // persist in db
            $manager->persist($child);
            // get a reference
            $ref = 'child_'.$i;
            $this->setReference($ref, $child);
            self::$refs[] = $ref;
        }
    }
}
