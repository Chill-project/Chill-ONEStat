<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ONEStatBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QualificationControllerTest extends WebTestCase
{
        /**
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    protected $client;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var \Chill\ONEStatBundle\Entity\Qualification
     */
    protected $qualification;
    
    public function setUp()
    {
        self::bootKernel();
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'center a_social',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
        
        $container = self::$kernel->getContainer();
        
        $this->em = $container->get('doctrine.orm.entity_manager')
                ;
        
        $qualifications = $this->em->createQuery("SELECT q "
                . "FROM ChillONEStatBundle:Qualification q "
                . "JOIN q.person p "
                . "JOIN p.center c "
                . "WHERE c.name LIKE :center_name ")
                ->setParameter('center_name', 'Center A')
                ->setMaxResults(50)
                ->getResult();
        
        $this->qualification = $qualifications[array_rand($qualifications)];
    }
    
    public function testList()
    {
        $client = $this->client;

        $client->request('GET', '/fr/person/'
                .$this->qualification->getPerson()->getId().'/qualification');
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode(),
                "the list page have a status code 200");
    }
    
    public function testCreate()
    {
        $client = $this->client;
        
        $nbBefore = $this->em->createQuery('SELECT COUNT(q.id) '
                . 'FROM ChillONEStatBundle:Qualification q '
                . 'WHERE q.person = :person ')
                ->setParameter('person', $this->qualification->getPerson())
                ->getSingleScalarResult();
        
        $crawler = $client->request('GET', '/fr/person/'
                .$this->qualification->getPerson()->getId().'/qualification/new');
        
        $button = $crawler->selectButton('Créer');
        
        $this->assertEquals(1, $button->count());
        
        $form = $button->form();
        
        $client->submit($form, array(
            'qualification' => array(
                'qualification' => 'brevet',
                'since' => '15-04-2016'
            )
        ));
        
        $this->assertTrue($client->getResponse()->isRedirect('/fr/person/'.
                $this->qualification->getPerson()->getId().'/qualification'),
                "test that submitting a form on create redirect to list of qualificaiton");
        
        // count the number of qualificaiton. 
        $nbAfter = $this->em->createQuery('SELECT COUNT(q.id) '
                . 'FROM ChillONEStatBundle:Qualification q '
                . 'WHERE q.person = :person ')
                ->setParameter('person', $this->qualification->getPerson())
                ->getSingleScalarResult();
        
        $this->assertEquals($nbBefore + 1, $nbAfter,
                "test that a qualfication has been created");
    }
    
    public function testEdit()
    {
        $client = $this->client;
        
        $crawler = $client->request('GET', '/fr/person/'
                .$this->qualification->getPerson()->getId().'/qualification/'.
                $this->qualification->getId().'/edit');
        
        $button = $crawler->selectButton('Mettre à jour');
        
        $this->assertEquals(1, $button->count());
        
        $form = $button->form();
        
        $client->submit($form, array(
            'qualification' => array(
                'qualification' => 'assimil',
                'since' => '19-04-2016'
            )
        ));
        
        $this->assertTrue($client->getResponse()->isRedirect('/fr/person/'.
                $this->qualification->getPerson()->getId().'/qualification'));
        
        $qualification = $this->em->find('ChillONEStatBundle:Qualification', 
                $this->qualification->getId());
        
        $this->assertEquals('assimil', $qualification->getQualification());
        $this->assertEquals(
                '19-04-2016',
                $qualification->getSince()->format('d-m-Y')
                );
    }

}
