<?php

namespace Chill\ONEStatBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillONEStatExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->setParameter('chill.one-stat.customfield-school-slug', 
              $config['customfield_school_slug']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/reports_edd.yml');
        $loader->load('services/reports_cv.yml');
        $loader->load('services/voters.yml');
    }
    
    /* (non-PHPdoc)
      * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
      */
    public function prepend(ContainerBuilder $container) 
    {
        $this->prependAuthorization($container);
        $this->prependRoute($container);
        $this->prependFormTemplate($container);
    }
    
    /**
     * add route to route loader for chill
     * 
     * @param ContainerBuilder $container
     */
    protected function prependRoute(ContainerBuilder $container)
    {
        //add routes for custom bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillONEStatBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    /**
     * add authorization hierarchy
     * 
     * @param ContainerBuilder $container
     */
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array()
        ));
    }
    
    protected function prependFormTemplate(ContainerBuilder $container)  
    {
        $twigConfig = array(
            'form_themes' => array('ChillONEStatBundle:Form:fields.html.twig')
        );
        $container->prependExtensionConfig('twig', $twigConfig);
    }
}
