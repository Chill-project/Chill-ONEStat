<?php

namespace Chill\ONEStatBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_one_stat');

        $rootNode
              ->children()
                ->scalarNode('customfield_school_slug')
                ->isRequired()
                ->info("the slug of the custom field containing the school data.")
                ->end()
              ->end()
            ->end();

        return $treeBuilder;
    }
}
